﻿VTagSDKBeaconHandlerExample shows you how to use the VTag SDK along with some helper classes to be able to coordinate which V-Tag Gateway is the winner. 

The general problem is that with a Beacon message, more than one V-Tag Gateway can send in the same Beacon message. 

This project will demonstrate how to coordinate multiple V-Tag Gateways so that you always know which Gateway an Asset tag is closest to.

VTagSDKBeaconHandlerExample uses several data structures to coordinate this data effectively:

1) VTagManager - Part of the V-Tag SDK. Allows you to receive V-Tag Messages from the V-Tag Gateways as well as send out V-Tag Commands.

2) BeaconHandler - Keeps track of the "winner" beacon message which may have been sent in by multiple gateways. Only has two public methods AddSecurityIndication and GetSecurityIndicationsmaking the class very easy to utilize.

3) BeaconProcessor - Processes "winner" Beacon messages every 30 seconds. It also keeps track to see if the GatewayID has changed or if the Beacon message has no been seen for a while
