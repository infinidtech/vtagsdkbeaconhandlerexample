﻿using log4net;
using log4net.Config;
using System;
using System.Reflection;
using vtagmanager;
using vtagmanager.libs.messages;

namespace VTagSDKBeaconHandlerExample
{
    class Program
    {
        static void Main(string[] args)
        {
            VTagProgram program = new VTagProgram();
        }

        public class VTagProgram
        {
            VTagManager manager;
            int connectedCount = 0;
            private readonly ILog log;
            BeaconProcessor beaconProcessor;
            BeaconHandler beaconHandler;
            DataCache dataCache;
            public VTagProgram()
            {               
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new System.IO.FileInfo("App.config"));
                log = LogManager.GetLogger(typeof(Program));

                log.Info("Starting App...");

                this.manager = VTagManager.Instance;
                manager.ConnectionStatusEvent += Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent += Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent += Manager_NewCommandResponseEvent;
                manager.NewSensorReadingEvent += Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent += Manager_NewSensorStatisticEvent;
                manager.NewSecurityIndicationEvent += Manager_NewSecurityIndicationEvent;
                manager.NewTagEvent += Manager_NewTagEvent;

                //Add each Gateway you want to enable
                manager.AddGateway(VTagType.VTag, "192.168.1.78", 8422, "admin", "change", "first_gateway", "infinid", false);
                manager.AddGateway(VTagType.VTag, "192.168.1.88", 8422, "admin", "change", "second_gateway", "infinid", false);

                dataCache = new DataCache();
                //BeaconHandler keeps track of winner beacon messages.
                beaconHandler = new BeaconHandler();
                //BeaconProcessor processes "winner" beacon messages every 30 seconds
                beaconProcessor = new BeaconProcessor(dataCache, beaconHandler, log);
                beaconProcessor.Start();

                System.Console.ReadLine();

                manager.Dispose();

                manager.ConnectionStatusEvent -= Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent -= Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent -= Manager_NewCommandResponseEvent;
                manager.NewSensorReadingEvent -= Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent -= Manager_NewSensorStatisticEvent;
                manager.NewSecurityIndicationEvent -= Manager_NewSecurityIndicationEvent;
                manager.NewTagEvent -= Manager_NewTagEvent;               
            }

            private void Manager_NewSecurityIndicationEvent(SecurityIndication securityIndication)
            {
                log.Info(string.Format("********Got New Security Indication Event for Tag {0}, From Gateway:{1}", securityIndication.TagID, securityIndication.GatewayID));
                beaconHandler.AddSecurityIndication(securityIndication);
            }

            private void Manager_NewTagEvent(TagObservation readTag)
            {
                log.Info(string.Format("********Got New Tag Event:{0}", readTag.ToString()));
            }

            private void Manager_NewSensorStatisticEvent(SensorStatistic sensorStatistic)
            {
                log.Info(string.Format("********Got New SensorStatistic Event:{0}", sensorStatistic.ToString()));
            }

            private void Manager_NewSensorReadingEvent(SensorReading sensorReading)
            {
                log.Info(string.Format("********Got New SensorReading Event:{0}", sensorReading.ToString()));
            }

            private void Manager_NewCommandResponseEvent(QueuedCommand queuedCommand)
            {
                log.Info(string.Format("********Got New CommandResponse Event:{0}", queuedCommand.PrintOut()));
            }

            private void Manager_NewAlarmIndicationEvent(AlarmIndication alarmIndication)
            {
                log.Info(string.Format("********Got New Alarm Event:{0}", alarmIndication.ToString()));                
            }

            private void Manager_ConnectionStatusEvent(ConnectionStatus connectionStatus)
            {
                if (connectionStatus.IsConnected)
                    connectedCount++;
                else
                    connectedCount--;

                log.Info(string.Format("********Got New ConnectionStatus Event:{0}", connectionStatus.ToString()));

                if (connectionStatus.IsConnected)
                {
                    try
                    {
                        QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetSecurity("first_gateway", SecurityMode.on, -50);
                        bool ran = manager.RunCommand(queuedCommand);
                        queuedCommand = VTagManager.VTagCommandFactory.SetSecurity("second_gateway", SecurityMode.on, -50);
                        manager.RunCommand(queuedCommand);
                    }
                    catch (Exception exc)
                    {
                        log.Error(string.Format("********************************************Got exception:{0}", exc.Message));
                    }
                }
            }
        }
    }
}
