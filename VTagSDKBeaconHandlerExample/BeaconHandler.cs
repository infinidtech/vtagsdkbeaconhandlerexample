﻿using System;
using System.Collections.Generic;
using System.Threading;
using vtagmanager.libs.messages;

namespace VTagSDKBeaconHandlerExample
{
    /// <summary>
    /// BeaconHandler keeps track of the "winner" beacon message
    /// which may have been sent in by multiple gateways. Only has two
    /// public methods AddSecurityIndication and GetSecurityIndications
    /// making the class very easy to utilize.
    /// </summary>
    public class BeaconHandler
    {
        private const int BEACON_THRESHOLD_SECONDS = 10;
        private const int IDLE_BEACON_THRESHOLD_SECONDS = 30;
        private const int WINNING_GATEWAY_THRESHOLD_COUNT = 3;
        public SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        private Dictionary<string, List<BeaconMessage>> beaconMessages = new Dictionary<string, List<BeaconMessage>>();
        private Dictionary<string, string> lastTagGatewayId = new Dictionary<string, string>();
        private Dictionary<string, DateTimeOffset> lastTagTimestamp = new Dictionary<string, DateTimeOffset>();
        private readonly Dictionary<string, GatewayCount> tagGatewayCount = new Dictionary<string, GatewayCount>();
        public BeaconHandler()
        {
        }

        #region Methods

        /// <summary>
        /// Add a new SecurityIndication
        /// </summary>
        /// <param name="securityIndication"></param>
        public void AddSecurityIndication(SecurityIndication securityIndication)
        {
            try
            {
                Semaphore.Wait();
                BeaconMessage message = GetCurrentBeaconMessage(securityIndication);
                message.AddSecurityIndication(securityIndication);
            }
            finally
            {
                Semaphore.Release();
            }
        }

        /// <summary>
        /// Get all SecurityIndications Winners.
        /// </summary>
        /// <returns>Only return SecurityIndications that have won the strength contest.</returns>
        public List<SecurityIndication> GetSecurityIndications()
        {
            try
            {
                Semaphore.Wait();
                //automatically filters out beacon messages that were weaker than other gateway' messages
                List<SecurityIndication> mySecurityIndications = new List<SecurityIndication>();
                foreach (string key in beaconMessages.Keys)
                {
                    List<BeaconMessage> tagBeaconMessages = beaconMessages[key];
                    bool done = false;
                    while (!done && tagBeaconMessages.Count > 0)
                    {
                        BeaconMessage tempMessage = tagBeaconMessages[0];
                        double seconds = (DateTimeOffset.Now - tempMessage.Received).TotalSeconds;
                        if (seconds > IDLE_BEACON_THRESHOLD_SECONDS)
                        {
                            tagBeaconMessages.RemoveAt(0);
                            SecurityIndication winner = tempMessage.Winner;
                            lastTagGatewayId[winner.TagID] = winner.GatewayID;
                            lastTagTimestamp[winner.TagID] = DateTimeOffset.Now;
                            int count = SetTagGateway(winner.TagID, winner.GatewayID);
                            if (count > WINNING_GATEWAY_THRESHOLD_COUNT)
                            {
                                mySecurityIndications.Add(winner);
                            }
                        }
                        else
                            done = true;
                    }
                }
                return mySecurityIndications;
            }
            finally
            {
                Semaphore.Release();
            }
        }

        /// <summary>
        /// Get the current beacon message for this time window. 
        /// </summary>
        /// <param name="securityIndication"></param>
        /// <returns>Current BeaconMessage</returns>
        private BeaconMessage GetCurrentBeaconMessage(SecurityIndication securityIndication)
        {
            List<BeaconMessage> tagMessages = GetByTagId(securityIndication.TagID);
            BeaconMessage currentMessage = null;
            for (int i = 0; i < tagMessages.Count; i++)
            {
                BeaconMessage message = tagMessages[i];
                int seconds = (securityIndication.Received - message.Received).Seconds;
                decimal absSeconds = Math.Abs(seconds);
                if (absSeconds < BEACON_THRESHOLD_SECONDS)
                {
                    currentMessage = message;
                    break;
                }
            }
            if (currentMessage == null)
            {
                currentMessage = new BeaconMessage() { TagID = securityIndication.TagID, Received = securityIndication.Received };
                tagMessages.Add(currentMessage);
            }
            return currentMessage;
        }

        private List<BeaconMessage> GetByTagId(String tagId)
        {
            if (!beaconMessages.ContainsKey(tagId))
            {
                beaconMessages.Add(tagId, new List<BeaconMessage>());
            }
            return beaconMessages[tagId];
        }

        /// <summary>
        /// Sets the gateway for a V-Tag. 
        /// </summary>
        /// <param name="tagId">V-Tag ID</param>
        /// <param name="gatewayId">Unique Gateway ID</param>
        /// <returns>Returns the number of consecutive times the gateway has stayed the same.</returns>
        private int SetTagGateway(string tagId, string gatewayId)
        {
            GatewayCount gatewayCount = null;
            if (!tagGatewayCount.ContainsKey(tagId))
            {
                gatewayCount = new GatewayCount(gatewayId, 0);
                tagGatewayCount.Add(tagId, gatewayCount);
            }
            else
            {
                gatewayCount = tagGatewayCount[tagId];

            }
            double seconds = (DateTimeOffset.Now - gatewayCount.LastBeacon).TotalSeconds;
            if (seconds > 3600)
            {
                //this beacon hasn't been seen for a long time so set the count back to 0
                gatewayCount.Count = 0;
            }
            gatewayCount.LastBeacon = DateTimeOffset.Now;
            if (gatewayCount.GatewayID == gatewayId)
            {
                gatewayCount.Count++;
            }
            else
            {
                gatewayCount.GatewayID = gatewayId;
                gatewayCount.Count = 1;
            }
            return gatewayCount.Count;
        }

        #endregion Methods

        /// <summary>
        /// Represents the BeaconMessage for a window of time. Multiple SecurityIndications can be found from more than one Gateway.
        /// </summary>
        class BeaconMessage
        {

            #region Properties

            public string TagID { get; set; }

            public DateTimeOffset Received { get; set; } = DateTimeOffset.Now;

            public string ReceivedString
            {
                get
                {
                    return Received.ToString("yyyy-MM-dd hh:mm:ss");
                }
            }

            private List<SecurityIndication> SecurityIndications { get; set; } = new List<SecurityIndication>();

            #endregion Properties

            #region Methods

            public SecurityIndication Winner
            {
                get
                {
                    SecurityIndication strongest = null;
                    foreach (SecurityIndication si in SecurityIndications)
                    {
                        if (strongest == null || si.RSSI > strongest.RSSI)
                            strongest = si;
                    }
                    return strongest;
                }
            }

            public void AddSecurityIndication(SecurityIndication si)
            {
                SecurityIndications.Add(si);
            }

            #endregion Methods

        }//BeaconMessage

        class GatewayCount
        {
            public GatewayCount(string gatewayID, int count)
            {
                GatewayID = gatewayID;
                Count = count;
            }

            public string GatewayID { get; set; }
            public int Count { get; set; }
            public DateTimeOffset LastBeacon = DateTimeOffset.Now;
        }

    }

}
