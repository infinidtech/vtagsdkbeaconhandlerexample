﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using vtagmanager.libs.messages;

namespace VTagSDKBeaconHandlerExample
{
    /// <summary>
    /// BeaconProcessor processes "winner" Beacon messages every 30 seconds. 
    /// It also keeps track to see if the GatewayID has changed or if the Beacon message has no been seen for a while
    /// </summary>
    public class BeaconProcessor
    {
        private readonly DataCache dataCache;
        private readonly BeaconHandler beaconHandler;
        private readonly ILog log;
        private const int BEACON_OLD_SECONDS = 3600;
        private const int WORK_TIMER_TICK_MS = 30000;
        Timer timer;
        public BeaconProcessor(DataCache dataCache, BeaconHandler beaconHandler, ILog log)
        {
            this.dataCache = dataCache;
            this.beaconHandler = beaconHandler;
            this.log = log;
        }

        public void Start()
        {
            log.Info("BeaconProcessor Starting Up");
            timer = new Timer(new TimerCallback(DoWork), null, 10000, 5000);
        }

        public void Stop()
        {
        }

        public async void DoWork(Object state)
        {
            try
            {
                //System.Diagnostics.Debug.WriteLine("*************************************************************************BEACONPROCESSOR.DOWORK.START");
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                List<SecurityIndication> securityIndications = beaconHandler.GetSecurityIndications();
                foreach (SecurityIndication current in securityIndications)
                {
                    log.Info(string.Format(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> BeaconProcessor.TagID={0}. GatewayID={1},Received={2},Now={3}", current.TagID, current.GatewayID, current.ReceivedString, DateTimeOffset.Now.ToString("yyyy-MM-dd hh:mm:ss")));

                    bool updateAssetLocation = false;

                    string lastWinnerGateway = dataCache.GetLastVTagGateway(current.TagID);
                    if (current.GatewayID != lastWinnerGateway)
                    {
                        //the winner gateway has changed since last beacon
                        //System.Diagnostics.Debug.WriteLine("lastVTagGatewayId has changed so process.");
                        updateAssetLocation = true;
                    }
                    else
                    {
                        //System.Diagnostics.Debug.WriteLine("***************************lastVTagGatewayId has NOT changed so process.");
                    }

                    DateTimeOffset? beaconLastSeen = dataCache.GetVTagBeaconLastSeen(current.TagID);
                    bool beaconOld = beaconLastSeen == null;
                    if (beaconLastSeen != null)
                    {
                        double seconds = (DateTimeOffset.Now - beaconLastSeen.Value).TotalSeconds;
                        beaconOld = seconds > BEACON_OLD_SECONDS;
                    }
                    if (beaconOld)
                    {
                        updateAssetLocation = true;
                    }

                    dataCache.SetLastVTagGateway(current.TagID, current.GatewayID);
                    dataCache.UpdateBeaconLastSeen(current.TagID);

                    if (updateAssetLocation)
                    {
                        log.Info(string.Format(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> New Gateway '{1}' Found for tag {0}", current.TagID, current.GatewayID));
                        //This Beacon message came in at a new Gateaway.
                    }

                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Got error in BeaconProcessor:DoWork-{0}",exc.Message));
            }
            finally
            {
                timer.Change(30000, 30000);
            }
        }//DoWork

    }
}
