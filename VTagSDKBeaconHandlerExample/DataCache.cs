﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTagSDKBeaconHandlerExample
{
    public class DataCache
    {
        Dictionary<string, string> lastVTagGateway = new Dictionary<string, string>();
        Dictionary<string, DateTimeOffset> vTagBeaconLastSeen = new Dictionary<string, DateTimeOffset>();

        public DataCache()
        {
        }

        public DateTimeOffset? GetVTagBeaconLastSeen(string vTagID)
        {
            if (vTagBeaconLastSeen.ContainsKey(vTagID))
                return vTagBeaconLastSeen[vTagID];
            return null;
        }

        public void UpdateBeaconLastSeen(string vTagID)
        {
            vTagBeaconLastSeen[vTagID] = DateTimeOffset.Now;
        }

        public string GetLastVTagGateway(string vtagID)
        {
            return lastVTagGateway.ContainsKey(vtagID) ? lastVTagGateway[vtagID] : null;
        }

        public void SetLastVTagGateway(string vTagID, string gatewayID)
        {
            lastVTagGateway[vTagID] = gatewayID;
        }
    }
}
